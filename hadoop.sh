#!/bin/bash
# 
# Hadoop installer for linux
#
# Author: Lorenzo Giustozzi <giustozzi@lorenzo.im> 
#
#
####################################################################################
# CONFIGURATION 

installation_path="/opt/hadoop"

# DO NOT EDIT

profile_path="/home/"$(whoami)"/.profile"
authorizedkeys_path="/home/"$(whoami)"/.ssh/authorized_keys"
ssh_path="/home/"$(whoami)"/.ssh"
sshkey_filenamehead="hadoop_rsa"
env_separator="#+++++++++#"
sed_conf_regex="/<configuration>/,/<\/configuration>/d"
install_logfile="install.log"
errinstall_logfile="error-install.log"
hadoop="org.apache.hadoop.yarn.server.resourcemanager.ResourceManage"

######################################################

usage="Usage: $0 <command>

Commands availaible:
install		install Hadoop 2.6 in $installation_path
uninstall	uninstall and remove completely Hadoop 2.6 from the system
clean		reset configuration to default
start		start Hadoop instance
status		get status"

# HADOOP DOWNLOAD INFO
hadoop_repo=( apache.panu.it mirror.nohup.it mirrors.muzzy.it/apache it.apache.contactlab.it apache.fastbull.org )
hadoop_repo_path="/hadoop/common/hadoop-2.6.0/"
hadoop_filename="hadoop-2.6.0.tar.gz"

# APPEND FILE CONTENT 
# .profile
profile_append="$env_separator
export YARN_HOME=$installation_path
export PATH_OLD=$PATH
export PATH=$installation_path/bin:$installation_path/sbin:$PATH
export HADOOP_HOME=$installation_path
export HADOOP_MAPRED_HOME=$installation_path
export HADOOP_COMMON_HOME=$installation_path
export HADOOP_HDFS_HOME=$installation_path
export HADOOP_CONF_DIR=$installation_path/etc/hadoop
$env_separator"

# hdf-site.xml
hdfssite_append="<configuration>
<property>
<name>dfs.replication</name>
<value>1</value>
</property>
<property>
<name>dfs.namenode.name.dir</name>
<value>file:/home/$(whoami)/yarnData/namenode</value>
</property>
<property>
<name>dfs.datanode.data.dir</name>
<value>file:/home/$(whoami)/yarnData/datanode</value></property>
</configuration>"

# core-site.xml
coresite_append="<configuration>
<property>
<name>fs.default.name</name>
<value>hdfs://localhost:9000</value>
</property>
</configuration>"

# yarn-site.xml
yarnsite_append="<configuration>
<property>
<name>yarn.nodemanager.aux-services</name>
<value>mapreduce_shuffle</value>
</property>
<property>
<name>yarn.nodemanager.aux-
services.mapreduce_shuffle.class</name>
<value>org.apache.hadoop.mapred.ShuffleHandler</value>
</property>
</configuration>"

# mapred-site.xml
mapredsite_append="<configuration>
<property>
<name>mapreduce.framework.name</name>
<value>yarn</value>
</property>
</configuration>"

####################################################################################

if [ "$#" -ne 1 -o "$1" == "-help" -o "$1" == "--help" -o "$1" == "-h" -o "$1" == "--h" ] || [ $1 != "install" -a $1 != "uninstall" -a $1 != "clean" -a $1 != "start" -a $1 != "stop"  -a $1 != "status" ]; then
	echo "$usage"
	exit -1
fi

if [[ $1 == "install" ]]; then
	#before to install do an hard clean
	$0 uninstall
	echo -n "Checking for java installation......"
	if type -p java; then
		_java=java
	elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]];  then
	   	_java="$JAVA_HOME/bin/java"
	    	echo -n $_java"\n"
	else
	    	echo "not found"
	   	echo "Java is requested to install Hadoop, aborting."
	    	exit -1; 	
	fi

	echo -n "Checking for java version..........."
	if [[ "$_java" ]]; then
	    	version=$("$_java" -version 2>&1 | awk -F '"' '/version/ {print $2}')
	    	echo "$version"
	    	if [[ "$version" < "1.7" ]]; then
			echo "Java version 1.7+ is required to install Hadoop, aborting."
			exit -1;
	    	fi
	fi


	sudo mkdir -p $installation_path
	sudo chown -R $(whoami):$(whoami) $installation_path
	cd $installation_path

	if [ $(sudo updatedb; grep -m1 "hadoop" <(sudo locate hadoop-2.6.0.tar.gz)) ]; then 
		echo "Hadoop found in $(grep -m1 "hadoop" <(sudo locate hadoop-2.6.0.tar.gz))), copying to working directory"
		cp $(grep -m1 "hadoop" <(sudo locate hadoop-2.6.0.tar.gz)) .
	fi

	if [[ -f $hadoop_filename ]]; then
		while true; do
	    		read -p "Hadoop source seems to be already present, do you want to redownload it from trusted source?" yn
	    		case $yn in
				[Yy]* ) rm $hadoop_filename; break;;
				[Nn]* ) break;;
				* ) echo "Please answer yes or no.";;
	    		esac
		done
	fi

	if [[ ! -f $hadoop_filename  ]]; then

		echo "Downloading hadoop source......"
		for i in ${hadoop_repo[@]}; do
			echo "* Trying repository $i"
			wget --progress=bar:force http://$i$hadoop_repo_path$hadoop_filename 2>$errinstall_logfile
			if [ -f $hadoop_filename  ]; then
		    		break;
			fi	
		done
	fi

	if [[ ! -f $hadoop_filename ]]; then
		echo "Error: can't download Hadoop Source, aborting."
		exit -1	
	fi

	echo -n "Extracting..........."
	tar -xzf $hadoop_filename -C .
	mv hadoop-2.6.0/* .
	rm -rf hadoop-2.6.0
	echo "ok"

	echo "Adding persistent ENV variables to /home/$(whoami)/.profile"
	echo "$profile_append" >> $profile_path

	sed -i.bak 's/${JAVA_HOME}/'"$(sed -e 's/\//\\\//g' <(echo $JAVA_HOME))/" $installation_path/etc/hadoop/hadoop-env.sh
	
	echo -n "Creating yarnData directories................"
	mkdir -p ~/yarnData
	mkdir -p ~/yarnData/namenode
	mkdir -p ~/yarnData/datanode
	echo "ok"	

	echo -n "Updating hdfs-site.xml......................."
	sed -i.bak $sed_conf_regex $installation_path/etc/hadoop/hdfs-site.xml 1>/dev/null
	echo "$hdfssite_append" >> $installation_path/etc/hadoop/hdfs-site.xml
	echo "ok"	

	echo -n "Formatting hdfs vfs.........................."
	$installation_path/bin/hdfs namenode -format &>install.log
	echo "ok"	

	echo -n "Updating core-site.xml......................."
	sed -i.bak $sed_conf_regex $installation_path/etc/hadoop/core-site.xml 1>/dev/null
	echo "$coresite_append" >> $installation_path/etc/hadoop/core-site.xml
	echo "ok"	

	echo -n "Updating yarn-site.xml......................."
	sed -i.bak $sed_conf_regex $installation_path/etc/hadoop/yarn-site.xml 1>/dev/null
	echo "$yarnsite_append" >> $installation_path/etc/hadoop/yarn-site.xml	
	echo "ok"	

	echo -n "Creating mapred-site.xml....................."
	cp $installation_path/etc/hadoop/mapred-site.xml.template $installation_path/etc/hadoop/mapred-site.xml 2>$errinstall_logfile
	sed -i.bak $sed_conf_regex $installation_path/etc/hadoop/mapred-site.xml 1>/dev/null
	echo "$mapredsite_append" >> $installation_path/etc/hadoop/mapred-site.xml
	echo "ok"		

	echo -n "Enabling passwordless authentication........."
	ssh-keygen -t rsa -N "" -f $ssh_path/$sshkey_filenamehead 1> $install_logfile
authorizedkeys_append="$env_separator
$(cat ~/.ssh/$sshkey_filenamehead.pub)
$env_separator"
	echo "$authorizedkeys_append" >> ~/.ssh/authorized_keys
	echo "ok"	

	echo "You are almost done, to complete installation type: \"source $profile_path\""

elif [ $1 == "clean" ]; then
	exit 1;
elif [ $1 == "start" ]; then
	if (type start-dfs.sh&>/dev/null); then 
		start-dfs.sh
		start-yarn.sh
	else 
		echo "Hadoop seems not to be installed"
	fi 2> /dev/null
	exit 1;
elif [ $1 == "stop" ]; then
	if (type start-dfs.sh&>/dev/null); then 
		stop-dfs.sh
		stop-yarn.sh
	else 
		echo "Hadoop seems not to be installed"
	fi 2> /dev/null
	exit 1;
elif [ $1 == "status" ]; then 
	if ps ax | grep -v grep | grep $hadoop &> /dev/null; then
		echo "running"
	else
		echo "stopped"
	fi
else
	#remove export's appended to profile
	sed -i.bak "/$env_separator/,/$env_separator/d" $profile_path 1>/dev/null
	#remove ssh key
	sed -i.bak "/$env_separator/,/$env_separator/d" $authorizedkeys_path 1>/dev/null
	rm $ssh_path/$sshkey_filenamehead* 2>/dev/null
	#find $installation_path/ -type f ! -regex ".*\($hadoop_filename\|$0\)$" -delete 2>/dev/null
	sudo rm -rf $installation_path
	rm -rf ~/yarnData
	rm -rf ~/yarnData/namenode
	rm -rf ~/yarnData/datanode
	echo "Hadoop sucesfully uninstalled"
fi

#
# Hadoop's project needs a specific classpath file that can be generated from the default one, just executing:
# 
# sed "s/YOUR_PATH_TO_YARN\/hadoop-2.6.0/$(sed -e 's/\//\\\//g' <(echo $YARN_HOME))/" PATH_TO_CLASSPATH_DEFAULT/classpath > PATH_TO_PROJECT/.classpath
#
# in this way we serve all the dependencies that an hadoop project should need to work properly
#
#
#
#

