# Java installer for debian jessie
#
# Author: Lorenzo Giustozzi <giustozzi@lorenzo.im> 
#
#
####################################################################################
# CONFIGURATION 
installation_path=/opt

# JAVA's #################################################

params="--progress=bar:force --header \"Cookie: oraclelicense=accept-securebackup-cookie\"" 
base_url="http://download.oracle.com/otn-pub/java/jdk"

ftemplate="%java_type%-%java_version%-linux-%platform%.tar.gz"
jtemplate="wget $params $base_url/%build_version%/$ftemplate"
dtemplate="%java_type%1.%a%.0_%b%"

bversion8="8u45-b14"
bversion7="7u79-b15"

# MESSAGES ###############################################

choose_java="Select a java version from the list:
1. JRE oracle-java7
2. JRE oracle-java8
3. JDK oracle-java7
4. JDK oracle-java8
[#]:"

usage="Usage: $0 <command>

Commands availaible:
install		install java in $installation_path
uninstall	uninstall and remove completely java from the system
status		list informations about java version actually installed"

##########################################################

if [ "$#" -ne 1 -o "$1" == "-help" -o "$1" == "--help" -o "$1" == "-h" -o "$1" == "--h" ] || [ $1 != "install" -a $1 != "uninstall" -a $1 != "clean" -a $1 != "start" -a $1 != "stop"  -a $1 != "status" ]; then
	echo "$usage"
	exit -1
fi

if [[ $1 == "install" ]]; then

	# detect platform version
	[ $(grep "64" <(uname -m)) ] && platform="x64" || platform="i586"
	# ask for java version to install and fill url template
	while true; do
		read -p "$choose_java" resp
		case $resp in
			1|3)	jbuild=$bversion7
				jversion=$(sed 's/-.*$//' <(echo $bversion7))
				;;
			2|4)	jbuild=$bversion8
				jversion=$(sed 's/-.*$//' <(echo $bversion8))
				;;
			* ) echo "Please choose one from the list.";;
		esac
		case $resp in
			1|2) 	jtype="jre"
				break	
				;;
			3|4) 	jtype="jdk"
				break
				;;
		esac
	done

	java_url=$(sed -e "s/\%build_version\%/$jbuild/" -e "s/\%java_type\%/$jtype/" -e "s/\%java_version\%/$jversion/" -e "s/\%platform\%/$platform/" <(echo $jtemplate))

	sudo mkdir -p $installation_path/$jtype
	sudo chown $(whoami):$(whoami) $installation_path/$jtype
	cd $installation_path/$jtype

	java_filename=$(sed 's/\(.*\)\///' <(echo $java_url))
	if [ $(sudo updatedb; grep -m1 $java_filename <(sudo locate $java_filename)) ]; then 
		echo "Java found in $(grep -m1 $java_filename <(sudo locate $java_filename)), copying to working directory"
		sudo cp $(grep -m1 $java_filename <(sudo locate $java_filename)) .
	fi

	if [[ -f $java_filename ]]; then
		while true; do
	    		read -p "Java source seems to be already present, do you want to redownload it from trusted source?" yn
	    		case $yn in
				[Yy]* ) sudo rm $java_filename; break;;
				[Nn]* ) break;;
				* ) echo "Please answer yes or no.";;
	    		esac
		done
	fi

	if [[ ! -f $java_filename ]]; then
		echo "Downloading java source......"
		eval $java_url
	fi

	if [[ ! -f $java_filename ]]; then
		echo "Error: can't download Java, aborting."
		exit -1	
	fi

	echo -n "Extracting..............."
	sudo tar -zxf $java_filename -C .
	echo "ok"

	echo -n "Updating alternatives...."
	sudo update-alternatives --install /usr/bin/java java $installation_path/$jtype/${jtype}1.${jbuild:0:1}.0_${jbuild:2:2}/bin/java 10000 &>/dev/null
	sudo update-alternatives --install /usr/bin/javac javac $installation_path/$jtype/${jtype}1.${jbuild:0:1}.0_${jbuild:2:2}/bin/javac 10000 &>/dev/null	
	echo "ok"

elif [[ $1 == "uninstall" ]]; then
	
	while true; do
		read -p "$choose_java" resp
		case $resp in
			1|3)	jbuild=$bversion7
				jversion=$(sed 's/-.*$//' <(echo $bversion7))
				;;
			2|4)	jbuild=$bversion8
				jversion=$(sed 's/-.*$//' <(echo $bversion8))
				;;
			* ) echo "Please choose one from the list.";;
		esac
		case $resp in
			1|2) 	jtype="jre"
				break	
				;;
			3|4) 	jtype="jdk"
				break
				;;
		esac
	done
	sudo rm -rf $installation_path/$jtype/${jtype}1.${jbuild:0:1}.0_${jbuild:2:2}
	sudo update-alternatives --remove java /opt/$jtype/${jtype}1.${jbuild:0:1}.0_${jbuild:2:2}/bin/java
    	sudo update-alternatives --remove javac /opt/$jtype/${jtype}1.${jbuild:0:1}.0_${jbuild:2:2}/bin/javac

fi
