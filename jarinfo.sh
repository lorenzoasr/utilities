#!/bin/bash
# 
# JAR Info Extractor
#
# Author: Lorenzo Giustozzi <giustozzi@lorenzo.im> 
#
#
####################################################################################

usage="Usage: $0 <command>

Commands availaible:
manifest        show manifest content
list		show archive file list"

if [ "$#" -ne 2 -o "$1" == "-help" -o "$1" == "--help" -o "$1" == "-h" -o "$1" == "--h" ] || [ $1 != "list" -a $1 != "manifest" ]; then
        echo "$usage"
        exit -1
fi

if [[ $1 == "list" ]]; then
	$JAVA_HOME/bin/jar tf $2  
elif [[ $1 == "manifest" ]]; then
	unzip -p $2 META-INF/MANIFEST.MF 
fi



