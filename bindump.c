/*************************************************************************\
*                               BinDump                                   *
*                        ---------------------                            *
*  copyright         (C) 2009  Pietro "eddy22" Minniti                    *
*  email                : eddy22@hack4fun.org                             *
***************************************************************************
***************************************************************************
*                                                                         *
*   This program is free software; you can redistribute it and/or modify  *
*   it under the terms of the GNU General Public License as published by  *
*   the Free Software Foundation; either version 2 of the License, or     *
*   (at your option) any later version.                                   *
*                                                                         *
*  This program is distributed in the hope that it will be useful,        *
*  but WITHOUT ANY WARRANTY; without even the implied warranty of         *
*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the          *
*  GNU General Public License for more details.                           *
*                                                                         *
*  You should have received a copy of the GNU General Public License      *
*  along with this program; if not, write to the Free Software            *
*  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,             *
*  MA 02110-1301 USA                                                      *
*                                                                         *
\*************************************************************************/
#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>

#define BYTE 8

void die_with_error (char *msg, char *arg) {
	fprintf (stderr, msg, arg);
	exit (1);
}

int main (int argc, char *argv[])
{
	FILE *file;
	unsigned long int size;
	unsigned char byte;
	unsigned char bytes[BYTE];
	unsigned short bit;
	unsigned long int i;
	short int j, y;

	if (argc != 2)
		die_with_error ("Usage: %s file\n", argv[0]);

	if ((file = fopen (argv[1], "rb")) == NULL)
		die_with_error ("%s doesn't exist\n", argv[1]);

        /* file size */
	fseek (file, 0, SEEK_END);
	size = ftell (file);
	fseek (file, 0, SEEK_SET);

	for (i = 0; i < BYTE; i++)
		bytes[i] = 0;

	for (i = 0, y = 1; i < size; i++, y++) {
		if ((i % BYTE) == 0)
			printf ("%08lu ", i);
                /* read one byte at a time */
		fread (&byte, sizeof (byte), 1, file);
		bytes[i % BYTE] = byte;
		bit = sizeof (byte) * BYTE;
		for (j = bit - 1; j >= 0; --j) /* extrapolate the bits */
			printf ("%u", (byte >> j) & 1);
		printf (" ");
		if ((y % BYTE) == 0) {
			for (j = 0; j < BYTE; j++) {
				printf ("%c", (isprint (bytes[j])) ? bytes[j] : '.');
				bytes[j] = '\0';
			}
			printf ("\n");
		}
	}
        /* formatting latest line */
	if ((y % BYTE) != 0) {
		for (j = y % BYTE; j < BYTE + 1; j++)
			printf ("%8s ", "");
		for (j = 0; j < y % BYTE - 1; j++) {
			printf ("%c", (isprint (bytes[j])) ? bytes[j] : '.');
			bytes[j] = '\0';
		}
		printf ("\n");
	} else {
		printf ("%8s ", "");
		for (j = 0; j < BYTE - 1; j++) {
			printf ("%c", (isprint (bytes[j])) ? bytes[j] : '.');
			bytes[j] = '\0';
		}
		printf ("\n");
	}

	fclose (file);

	return 0;
}
