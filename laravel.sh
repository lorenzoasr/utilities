#!/bin/bash
# 
# Laravel installer 
# 
# This script install and uninstall Laravel5
#
# Author: Lorenzo Giustozzi <giustozzi@lorenzo.im> 
# Date: 24/05/2015
#
# Dependencies: 
# - composer: dependency manager for php
# - curl: tool to create HTTP connections from terminal line
#
#
####################################################################################
# CONFIGURATION 

installation_path="/home/$(whoami)"
logging="2>> error.log 1>> install.log"
sed_install="s/@/install/"
sed_remove="s/@/remove/"

# COMPOSER
composer_data="~/.composer"
composer_filename="composer"
composer_inst_path="/usr/local/bin/"
composer_baseurl="https://getcomposer.org/" 
composer_install_url=$composer_baseurl"installer"
install_composer="sudo curl -sS $composer_install_url | sudo php -- --install-dir=$composer_inst_path --filename=$composer_filename  $logging"
uninstall_composer="sudo rm $composer_inst_path/$composer_filename  &> /dev/null; rm -rf $composer_data;"

# APTGETTABLE
autoremove="sudo apt-get -y autoremove $logging"
apt_template="sudo apt-get -y @mode @packages $logging"
# $1 mode, $2 packages
function getapt(){ sed -e "s/@mode/$1/" -e "s/@packages/$2/" <(echo $apt_template); }

#CURL
curl_pkg="curl"
install_curl="$(getapt "install" "$curl_pkg")"
uninstall_curl="$(getapt "purge" "$curl_pkg")"

#APACHE
apache_pkg="apache2-mpm-prefork apache2-utils libexpat1 apache2-suexec"
install_apache2="$(getapt "install" "$apache_pkg")"
uninstall_apache2="$(getapt "purge" "$apache_pkg")"
apache2_postinst="sudo a2enmod headers $logging; sudo a2enmod expires $logging; sudo a2enmod rewrite $logging; sudo apache2ctl graceful $logging;"

#PHP5-APACHE
php5_pkg="php5 php5-cli php5-common libapache2-mod-php5 php5-mcrypt php5-json"
install_php5="$(getapt "install" "$php5_pkg")"
uninstall_php5="$(getapt "purge" "$php5_pkg")"
php5_postinst="sudo php5enmod mcrypt $logging; sudo service apache2 reload $logging;"

#MYSQL
MSQL_VER="5.5"
MSQL_PWD="andeleandele4rriba4rriba"
mysql_pkg="mysql-server mysql-client php5-mysql"
mysql_preconf="sudo debconf-set-selections <<< 'mysql-server-$MYSQL_VER mysql-server/root_password password $MYSQL_PWD'; sudo debconf-set-selections <<< 'mysql-server-$MSQL_VER mysql-server/root_password_again password $MYSQL_PWD';"
install_mysql="$(getapt "install" "$mysql_pkg")"
uninstall_mysql="$(getapt "purge" "$mysql_pkg")"

#LARAVEL
install_laravel="composer global require \"laravel/installer=~1.1\""
laravel_postinst="sudo ln -s $composer_data/vendor/bin/laravel /usr/local/bin;"
uninstall_laravel="sudo rm /usr/local/bin/laravel $logging"

#####################################################

laravel_vh_template="<VirtualHost *:80>
        ServerAdmin webmaster@localhost

        DocumentRoot /var/www/@projectname@/public
        <Directory />
                Options FollowSymLinks
                AllowOverride None
        </Directory>
        <Directory /var/www/@projectname@/public>
                Options Indexes FollowSymLinks MultiViews
                AllowOverride All
                Order allow,deny
                allow from all
        </Directory>

        ErrorLog /error.log

        # Possible values include: debug, info, notice, warn, error, crit,
        # alert, emerg.
        LogLevel warn

        CustomLog /access.log combined
</VirtualHost>"


#####################################################

usage="Usage: $0 <command>

Commands availaible:
install			install laravel
uninstall		remove laravel
addproject       	create a project
delproject		delete installed project"

####################################################################################
# $1 what
# $2 how
# $3 postinstall
function install(){
	echo -n "Checking for $1................"
    if ! (sudo which $1&>/dev/null); then         
		echo "not found"
       	echo -n "Installing..............." 
		eval $2
		if [ ! -z "$3" ]; then eval $3; fi 
       	if ! (sudo which $1&>/dev/null); then
           	echo "failed, aborting install"
           	exit -1;    
    	fi
	fi
	echo "ok"
}

####################################################################################

if [ "$#" -ne 1 -o "$1" == "-help" -o "$1" == "--help" -o "$1" == "-h" -o "$1" == "--h" ] || [ $1 != "install" -a $1 != "uninstall" -a $1 != "addproject" -a $1 != "delproject" ]; then
	echo "$usage"
	exit -1
fi

if [[ $1 == "install" ]]; then
	
	install "curl" "$install_curl"
	install "apache2" "$install_apache2" "$apache2_postinst"
	install "php5" "$install_php5" "$php5_postinst"	
	install "composer" "$install_composer"
	eval $mysql_preconf	
	install "mysql" "$install_mysql"
	echo PURGE | debconf-communicate mysql-server-5.5
	eval $install_laravel
	eval $laravel_postinst

elif [ $1 == "uninstall" ]; then
	echo "Removing active projects......."
	bash $0 delproject
	echo -n "Uninstalling...."
	eval $uninstall_curl
	eval $uninstall_php5
	eval $uninstall_apache2
	eval $uninstall_mysql
	eval $autoremove
	eval $uninstall_composer
	eval $uninstall_laravel
	echo "ok!"

elif [ $1 == "addproject" ]; then
	if ! $(type laravel &>/dev/null); then
		echo "Laravel must be installed, aborting"
		exit -1;
	fi
	echo -n "Choose a project name: "
	read PROJNAME
	cd $installation_path
	laravel new $PROJNAME
	sudo chgrp -R www-data $PROJNAME
	sudo chmod -R 775 $PROJNAME/storage
	sudo chmod -R 775 $PROJNAME/vendor	
	sudo ln -s $installation_path/$PROJNAME /var/www/$PROJNAME
	laravel_vh=$(echo "$laravel_vh_template" | sed "s/@projectname@/$PROJNAME/")
	sudo sh -c "echo \"$laravel_vh\" >> /etc/apache2/sites-enabled/$PROJNAME.laravel.conf"
	sudo sh -c "a2dissite default public 000-default $logging"
	sudo service apache2 restart $logging	

elif [ $1 == "delproject" ]; then
	projname=$(ls /etc/apache2/sites-enabled/ | grep .laravel | sed 's/.laravel//')
	if [ -z "$projname" ]; then
		echo "Project not found.."
		exit -1
	fi
	sudo rm -rf /var/www/$projname
	sudo rm -rf ~/$projname
	sudo rm /etc/apache2/sites-enabled/$projname.laravel
fi

#
# LAMP: http://guide.debianizzati.org/index.php/Installare_un_ambiente_LAMP:_Linux,_Apache2,_SSL,_MySQL,_PHP5
# MYSQL nopassword installation trick: http://askubuntu.com/questions/79257/how-do-i-install-mysql-without-a-password-prompt
# COMPOSER: https://getcomposer.org
#
#
#

