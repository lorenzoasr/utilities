#!/bin/bash
# 
# Script name
# Script description
#
# Author: Lorenzo Giustozzi <giustozzi@lorenzo.im> 
# Date: 
#
#
####################################################################################
# CONFIGURATION 

installation_path="/opt/hadoop"

######################################################

usage="Usage: $0 <command>

Commands availaible:
install			command info
uninstall		command info"

####################################################################################

if [ "$#" -ne 1 -o "$1" == "-help" -o "$1" == "--help" -o "$1" == "-h" -o "$1" == "--h" ] || [ $1 != "command1" -a $1 != "command2" ]; then
	echo "$usage"
	exit -1
fi

if [[ $1 == "install" ]]; then
	exit 1;
elif [ $1 == "clean" ]; then
	exit 1;
fi

