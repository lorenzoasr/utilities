# Enjoy miner
#
# Author: Lorenzo Giustozzi <giustozzi@lorenzo.im> 
#
#
####################################################################################

params_template="--header='Cookie: PHPSESSID=cookie%city%'" 
base_url="https://enjoy.eni.com"


usage="Usage: $0 <city>

Cities availaible: firenze,milano,torino,roma"

if [ "$#" -ne 1 -o "$1" == "-help" -o "$1" == "--help" -o "$1" == "-h" -o "$1" == "--h" ] || [ $1 != "roma" -a $1 != "torino" -a $1 != "milano" -a $1 != "firenze" ]; then
	echo "$usage"
	exit -1
fi

params=$(sed -e "s/\%city\%/$1/" <(echo $params_template))

set_template="wget $params $base_url/%city%/trova_auto"
get_template="wget $params $base_url/get_vetture -O %city%.json"

set_url=$(sed -e "s/\%city\%/$1/" <(echo $set_template))
get_url=$(sed -e "s/\%city\%/$1/" <(echo $get_template))
echo "Executing: $set_url"
eval $set_url &>/dev/null

echo "Executing: $get_url"
eval $get_url &>/dev/null
